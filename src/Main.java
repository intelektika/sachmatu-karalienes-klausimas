import objects.Chess_Board;

public class Main {
    public static void main(String[] args) {
        Chess_Board chessBoard = new Chess_Board(10);
        System.out.println(printInfo(chessBoard));

        if(!chessBoard.queenRecursion(0)) {
            System.out.println("ERROR: N-Queen question could not be solved!");
            System.out.println("Try changing the board size and seeing if the problem persists.");
        } else {
            System.out.println(printInfo(chessBoard));
        }
    }

    private static String printInfo(Chess_Board chessBoard) {
        return chessBoard.getSize() + " x " + chessBoard.getSize() + " chess board.\r\n" +
                "Queen count: " + chessBoard.getQueenCount() + "!\r\n" +
                chessBoard.printBoard();
    }
}
