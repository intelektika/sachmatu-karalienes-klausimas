package objects;

public class Chess_Board {
    private final int size;
    private int queenCount;
    private final char[][] board;

    /**
     * Chess_Board class object constructor with size parameter
     * @param size Defines the X and Y size of the board.
     */
    public Chess_Board(int size) {
        this.size = size;
        this.queenCount = 0;
        this.board = new char[size][size];
        initBoard();
    }

    /**
     * Initializes the chess board by placing '_' symbols on every tile.
     */
    private void initBoard() {
        for(int y = 0; y < getSize(); y++)
            for(int x = 0; x < getSize(); x++)
                getBoard()[x][y] = '_';
    }

    /**
     * Checks if it is safe to place the queen piece at the given tile on the left side of the board.
     * @param row y axis
     * @param col x axis
     * @return Whether the queen can be safely placed on the given tile.
     */
    private boolean isSafe(int row, int col) {
        int i, j;
        for(i = 0; i < col; i++)
            if(getBoard()[row][i] == 'Q')
                return false;

        for(i = row, j = col; i >= 0 && j >= 0; i--, j--)
            if(getBoard()[i][j] == 'Q')
                return false;

        for(i = row, j = col; j >= 0 && i < this.size; i++, j--)
            if(getBoard()[i][j] == 'Q')
                return false;

        return true;
    }

    /**
     * Recursive method that checks and places every available spot for a queen piece on the left side of the board.
     * @param col Starting column which is used for placing queen pieces. By default, this is set to 0.
     * @return Whether the recursive method was successful in placing all the queen pieces. If the method returns false - it failed.
     */
    public boolean queenRecursion(int col) {
        if(col >= this.size)
            return true;

        for(int i = 0; i < this.size; i++) {
            if(isSafe(i, col)) {
                getBoard()[i][col] = 'Q';
                this.queenCount++;

                if(queenRecursion(col + 1))
                    return true;

                this.queenCount--;
                getBoard()[i][col] = '_';
            }
        }

        return false;
    }

    /**
     * Creates a string variable of the entire board.
     * @return Chess board organized by rows and columns.
     */
    public String printBoard() {
        StringBuilder output = new StringBuilder();
        for(int y = 0; y < getSize(); y++) {
            for (int x = 0; x < getSize(); x++) {
                output.append(getBoard()[x][y]);
                if(x != getSize() - 1)
                    output.append(" ");
            }
            output.append("\r\n");
        }

        return output.toString();
    }

    public int getSize() { return size; }
    public int getQueenCount() { return queenCount; }
    public char[][] getBoard() { return board; }
}
